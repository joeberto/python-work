#===============================================================================
# P3_Controller.py
# Starter code by Anthony Hornof - 5-2-2016
# Rest by Joseph Hill 5-17-2016
# Version 0.2
# Implementation for Spring 2016 CIS 211 Project 2
# Derived in part from Dave Kieras' design and implementation of EECS 381 Proj 4
#===============================================================================

import sys  # for argv


import P4_Model
from P4_Utility import *
import P4_View

#===============================================================================
def main ():
#===============================================================================
# Create a Model object and assign it to a global variable called "self.__the_model"
# Create a local instance of a Controller and have it start the run() function.



    c = Controller()
    c.run()

#===============================================================================
class Controller:
#===============================================================================
    '''
    The controller object handles user keyboard input and provides textual to
    the console.  It follows the model view-controller software design pattern.
    '''

    #===========================================================================
    def __init__(self):

        # Local "private" member variables

        # File with list of commands to be executed (initialized to default).
        self.__input_filename = "commands.txt"

        # File object currently being read from.  This is also used to indicate
        #   whether the system is currently getting lines from the user or a file.
        self.__input_file_object = None

        #
        #global self.__the_model
        self.__the_model = P4_Model.the_model
        #global self.__the_view
        # Instantiate View
        self.__the_view = P4_View.View()
        # Attach view to model
        self.__the_model.attach_view(self.__the_view)

    #===========================================================================
    def run(self):
    #===========================================================================
        '''
        () -> None.
        Process the command lines for the human-robot simulation.
        '''

        print("Starting Human-Robot Interaction Simulation.")

        # Attempt to open an input file for an initial set of commands
        self.open_initial_input_file()

        #=======================================================================
        # Command loop
        while True:

            try:
                # Get the next line of input whether it is from the user or a file.
                line = self.get_next_input_line()
                line_list = line.lower().split()

                # If there are no arguments to process, just loop.
                if len(line_list) == 0:
                    continue    # Loop to the top of the while loop.

                #=======================================
                # In this loop, make sure that:
                # 1. The first word of the command line is viable, such as that the
                #    command truly exists, or the human truly exists.
                # 2. The number of arguments that come after the first word are the
                #    correct number that would be needed given the first word, or
                #    provide at least the minimum number of arguments that could be
                #    needed based on the first word in the line.

                # If the command is...


                #if its 'go'
                if line_list[0]== 'go':
                    #check if its just 'go'
                    if len(line_list) > 1:

                        raise BadLineError
                    else:
                        self.__the_model.update()

                #=======================================
                # '<human> or <robot>' command - tell a human or robot object to do something
                elif self.__the_model.get_human(line_list[0]) or self.__the_model.get_robot(line_list[0]):
                    self.do_human_robot_command(line_list)


                # if command is 'show'
                elif line_list[0]=='show':
                    #check to see if world is created
                    if self.__the_model.get_world_size() == None:
                        raise BadMsgError('World not created yet.')
                        #print('World not created yet.')
                    #else it is, draw the view
                    else:
                        self.do_show_command()

                #=======================================
                # 'create'
                elif line_list[0]=='create' and len(line_list) > 2:     # Change 2 to 3 for P3.
                    # Send the arguments to the Model's create object function.
                     self.__the_model.create_sim_object(line_list[1:])
                        # If an error was returned.
                        #print("Unrecognized command:", line)

                #=======================================
                # 'status'                      (Make sure there are NO args.)
                elif line_list[0]=='status' and len(line_list) == 1:
                    # Get and print a text string from the Model of each
                    #   simulation object describing itself.
                    self.__the_model.describe_all( )

                #=======================================
                # 'open'
                elif line_list[0]=='open' and len(line_list) == 2:
                    # Overwrite the input-filename member-variable.
                    self.__input_filename = line_list[1]
                    # Attempt to open the file.
                    self.open_input_file()

                #=======================================
                # 'quit'
                # Prompt again and quit.
                elif line_list[0] == 'quit' and line_list[1:]==[]:
                    print("Are you sure you want to quit? (Y/N) ", end='')
                    line = self.get_next_input_line()
                    if line.lower() == 'y':
                        break


                #=======================================
                # 'q'
                # A hidden convenient fast way to quit.  Remove before distributing or testing.
                elif line_list[0] == 'q' and line_list[1:]==[]:
                    break

                #=======================================
                # Unrecognized command
                else:
                    raise BadLineError
                    #print("Unrecognized command:", line)



            except BadLineError as err:
                print(err,line )

            except BadMsgError as err:
                print(err)

    #===========================================================================
    # Manage the command line input file
    #===========================================================================

    def get_next_input_line(self):
        '''
        ( ) -> string
        • Displays the prompt.
        • Returns the next line to be processed, or '' if there is no line.
        • Gets the next line of text either from an input file or from the user,
          depending on the current setting of current_input_mode.
        • When reading from an input file, and either a blank line or an end of file
          is encountered, close the input file and set the file object var to None.
        '''

        # If there is currently a command-line input file open, then use it.
        if self.__input_file_object:

            # Get the line.
            getline = self.__input_file_object.readline().strip()

            # If it is not empty, show it and return it.
            if getline:
                print ('Time',self.__the_model.get_time(),"FILE>", getline)
                return getline
            else:
                # Else close the file and set self.__input_file_object to None.
                self.__input_file_object.close()
                self.__input_file_object = None
                print ("Closing file.")
                return ''

        # Else, input is currently being provided by the user's keystrokes.
        else:

            # Issue the prompt
            print('Time',self.__the_model.get_time(),"> ", end='')

            # Get and return the command line from the user.
            return input()


    #===========================================================================
    def open_initial_input_file (self):
        '''
        Attempt to open a file for an initial set of commands.
        ( ) -> None
        If a filename was entered as a command line argument, overwrite the
          controller's member variable with that new filename.
        '''

        # If a filename was entered as an argument, overwrite the default initial
        #   input filename with this new filename.
        if len(sys.argv) > 1 and sys.argv[1]:
            self.__input_filename = sys.argv[1]
        # Attempt to open the input file.
        self.open_input_file( )


    #===========================================================================
    def open_input_file (self):
        '''
        ( ) -> None
        Attempts to open the filename in the input file member variable to
          execute a set of commands.
        '''

       # If an initial file exists and is readable.
        #if (os.path.isfile( self.__input_filename ) and
               #os.access( self.__input_filename, os.R_OK)):
        try:
            # Then open that file.
            print ("Reading file:", self.__input_filename)
            self.__input_file_object = open ( self.__input_filename )

        except(FileExistsError,FileNotFoundError):
            # Else error.
            print ("Error: Could not open and read input file:", self.__input_filename)


    #===========================================================================
    # Execute commands
    #===========================================================================

    def do_human_robot_command(self, args):
        '''
        Parameters: args, a list of arguments that is already confirmed to be
                          nonempty with the first argument a human or robot in the model.
        Returns:    None (All errors are reported within, so no need to return
                          an error flag.)

        Processes the remainder of the arguments to insure that they at least
        represent valid locations on the map.  If they are valid,
        call the appropriate function calls in the model to build them.
        '''
        #check which if its human or robot, assign current_obj to it
        if self.__the_model.get_human(args[0]) != None:
            #keep a pointer to the human
            current_obj = self.__the_model.get_human(args[0])

        elif self.__the_model.get_robot(args[0]) != None:
            #Keep a pointer to the robot
            current_obj = self.__the_model.get_robot(args[0])


        # Make sure the rest of the command is in a correct form.

        #=======================================================================
        # 'move' command
        if len(args) > 1 and args[1] == 'move':
            temp_list = args[2:]
            destination_list = []

            for i in temp_list:
               #check single characters
                if len(i) == 1:
                    #if it is a letter
                    if i.isalpha():
                        #extract location from waypoint
                        destination_list.append(self.__the_model.get_waypoint_location(i))
                        #print(i)
                    #else it isnt a letter, error
                    else:
                        raise BadMsgError('Error: Invalid move command.')
                #check pairs of coordinates
                elif len(i)>1:
                    loc = i.split(',')
                    if self.__the_model.get_valid_location(tuple(loc)) == None:
                        raise BadMsgError('Error: '+ i +' is not an valid location for this "move"')
                    else:
                        destination_list.append(self.__the_model.get_valid_location(tuple(loc)))



            current_obj.journey_to(destination_list)
            new_loc = current_obj.get_next_moving_location()
            current_obj.move_to(new_loc)
                #else its a pair of ints



            # move <x> <y>
            #if len(args) == 4:
                # Create a location tuple from the final two args
                #location = (args[2], args[3])
            #else its move to a waypoint <destination>
            #elif len(args) == 3:
                #check if waypoint exists
                #if self.__the_model.get_object(args[2]):
                    #it exists, extract location
                    #location = self.__the_model.get_waypoint_location(args[2])

            #=======================================================================
            # Invalid move command.
            #else:
               # raise BadMsgError("Error: Invalid move command.")
                #print("Error: Invalid move command.")

            # If it is a valid location, move the object to it.
            #if location and self.__the_model.get_valid_location(location):
                # Then move the human to that location.
                #current_obj.move_to(self.__the_model.get_valid_location(location))

            #else:
                #raise BadMsgError("Error: Invalid location.")
                #print("Error: Invalid location.")

        else:
            raise BadMsgError("Error: Invalid human/robot command.")
            #print("Error: Invalid human/robot command.")

    def do_show_command(self):
        self.__the_view.draw()


#===============================================================================
main ()
#===============================================================================

