# P3_Model.py
# Joseph Hill 5-17-2016
# Version 0.2
# Implementation for Spring 2016 CIS 211 Project 2
# Derived in part from Dave Kieras' design and implementation of EECS 381 Proj 4
from P4_Utility import *

class View:

    def __init__(self):
        self.__world_size = None
        self.__objects = {}
        self.__waypoints = {}

    def create(self, world_size):
        """
        Called by the Model object after the users issues the “create world <n>” command. Sets
        a “private” member variable that keeps track of the view size. Creates the 2D grid (a list of lists) of
        strings that constitutes the map view. Each string in the 2D grid is a two- or four-character string that is
        filled, immediately after the user issues a “show” command, with the appropriate contents as described
        in the “Map View”
        """

        self.__world_size = world_size


    def update_object(self,name,location):
        """
        Called by the model to add or move a SimObject (other than Waypoints) on
        the map. Takes as arguments: a lowercase string that is the name of a non-Waypoint object; a tuple that
        is a valid map location. Calls the view’s __update( ) method to add or modify the View object’s
        collection of non-Waypoint SimObjects
        """
        #if location is None, remove object
        if location == None:
            del self.__objects[name]
        #else location is not None, update object
        else:
            self.__objects[name] = location


    def add_landmark(self,name,location):
        """
        Behaves exactly the same as update_object( ) but is used to add Waypoints
        to a separate collection of Waypoint SimObjects. These two separate collections are needed in order to
        place the Waypoints (the landmarks) in different map positions than the other SimObjects.
        """

        self.__waypoints[name] = location

    def draw(self):
        """
        Draws the map grid as specified in the “Map View”
        """
        nlist = list(self.__objects.values())

        for row in range(self.__world_size + 1):
            # If its a multiple of 5
            if (self.__world_size - row) % 5 == 0:
                print("%02d" % (self.__world_size - row), ' ', end='')
            # else print empty
            else:
                print('    ', end='')
            for col in range(self.__world_size + 1):
                if (col, (self.__world_size - row)) in self.__waypoints.values():

                    dot = list(self.__waypoints.keys())[list(self.__waypoints.values()).index((col, (self.__world_size - row)))]
                    dot = dot.upper()

                else:
                    dot = '.'
                # check if a an Objects location is on map, print if it is

                if (col, (self.__world_size - row)) in self.__objects.values():
                    # check if its repeated
                    if nlist.count((col, (self.__world_size - row))) >= 2:
                        print(dot,'*  ',sep='', end='')
                        #print('{} '.format(dot,'*'),end='')
                    # if (col,(self.__world_size-row)) in self.__objects
                    else:
                        name = list(self.__objects.keys())[list(self.__objects.values()).index((col, (self.__world_size - row)))]
                        print(dot,name[:1].capitalize(),'  ',sep='',end='')
                        #print(dot,'{} '.format(name[:1].capitalize()), end='')



                # if (col) == A[0] and (self.__world_size-row) == A[1]:
                # print('A   ',end='')
                # else print a dot
                else:
                    print(dot,'  ', end='')
            print()

        # setup for bottom row of numbers
        print('    ', end='')
        for col in range(self.__world_size+1):
            #print('00',end='')
            if (col) % 5 == 0:

                print("%02d" % (col), ' ', end='')
            else:
                print('    ', end='')
        print()



