#===============================================================================
# P3_Model.py
# Starter code by Anthony Hornof - 5-2-2016
# Rest by Joseph Hill 5-17-2016
# Version 0.2
# Implementation for Spring 2016 CIS 211 Project 2
# Derived in part from Dave Kieras' design and implementation of EECS 381 Proj 4
#===============================================================================
from P4_Utility import *

class Model:
    '''
    The Model object keeps track of everything in the simulated world.
    Only one Model should be created in each run of the simulation.
    '''

    #===========================================================================
    def __init__(self):
        self.__world_size = None
        self.__humans = []
        self.__robots = []
        self.__objects = []
        self.__view = None
        self._time = 0

    def get_time(self):
        #return time, int
        return self._time

    def update(self):
        #updates all simulation objects in the order which they were created. advance time by a minute.
        #for i in self.__humans:
           # i.set_moving()
           # i.update()


        self._time += 1


        pass

    def get_fire(self,name):
        # Same as get_human( ) and get_robot( ) but for Fire objects
        pass

    def fire_at_location(self,location):
        #Returns a Fire object at that location if one exists. Otherwise returns None
        pass

    def delete_fire(self,name):
        # Delete from the Model and the View a fire with the passed-in name
        pass
    #===========================================================================
    def __str__(self):
        return "The world is of size " + str(self.__size)

    #===========================================================================
    def get_world_size(self):
        return self.__world_size

    def attach_view(self,v):
        #attach view to Model's self.__view variable
        self.__view = v





    #===========================================================================
    def get_valid_location(self, arg1, arg2=None):
        '''
        Determine if a location is in the world.  If yes, returns a tuple of ints.
        This function is made polymorphic by using "switch on type".

        Parameters: arg1 and arg2 are ints, OR
                    arg1 and arg2 are strings, OR
                    arg1 is a tuple of two ints, and no arg2 is provided, OR
                    arg1 is a tuple of two strings, and no arg2 is provided
        Returns:    a tuple of ints if the location is in the world
                    None otherwise.

        Examples of use if the world is of size 30:
        self.get_valid_location(10, 20) -> (10, 20)
        self.get_valid_location('10', '20') -> (10, 20)
        self.get_valid_location((10, 20)) -> (10, 20)
        self.get_valid_location('a', '20') -> None
        self.get_valid_location(1.0, 20) -> None
        '''

        # Switch on type.  If arguments are...

        # (int, int)
        if type(arg1) == type(arg2) == int:
            x = arg1
            y = arg2

        # (str, str)
        elif type(arg1) == type(arg2) == str and arg1.isdigit() and arg2.isdigit():
            x = int(arg1)
            y = int(arg2)

        # (tuple of two ints)
        elif (arg2 == None and type(arg1) == tuple and len(arg1)==2 and
                type(arg1[0])==int and type(arg1[1])==int):
            x = arg1[0]
            y = arg1[1]

        # (tuple of two strings which can convert into digits)
        elif (arg2 == None and type(arg1) == tuple and len(arg1)==2 and
                type(arg1[0])==str and type(arg1[1])==str and
                arg1[0].isdigit() and arg1[1].isdigit()):
            x = int(arg1[0])
            y = int(arg1[1])

        # Arguments not handled, or invalid location.
        else:
            # print("Error: Model.get_valid_location() arguments not handled.")
            return None # The provided arguments are not handled.

        # If the location is in the world, return True.
        if (x >= 0 and y >= 0 and
                x <= self.__world_size and y <= self.__world_size):
            return (x, y)
        else:
            return None

    #===========================================================================
    def create_sim_object(self, arg_list):
        '''
        Create a simulation object based on the contents of the arg_list.
        Parameters: arg_list, list of strings entered after "create" command
        Returns:    True for if the line cannot be parsed, False if it can be.

        The only assumption that can be made about the arg_list when entering
        this function is that there was at least one string in the command line
        after "create".
        '''

        MIN_WORLD_SIZE = 5
        MAX_WORLD_SIZE = 30

        # Continue here checking for all of the different objects that "create"
        # could be called to build.  For each, after checking for the string
        # that appeared after "create", make sure that any additional arguments
        # on the line are all permissable given the project specification.


        #=======================================================================
        # World
        if arg_list[0]=='world':
            # Else return error.

            # Verify that there is one additional argument and it is an integer.
            if len(arg_list) == 2 and arg_list[1].isdigit():
                size = int(arg_list[1])
            else:
                raise BadLineError

            # If a world has aleady been created
            if (self.__world_size):
                # Then return an error.
                raise BadMsgError("Error: World already exists.")
                #print("Error: World already exists.")
                #return False  # Error was reported, so no need to return an error flag.

            # Verify size is in range.
            if size < MIN_WORLD_SIZE or size > MAX_WORLD_SIZE:
                raise BadMsgError("Error: World size is out of range.")
                #print ("Error: World size is out of range.")
            else:
                # Else create the world.
                print('Creating world of size ', size, '.', sep='')
                self.__world_size = size
                #View.create
                self.__view.create(self.__world_size)



        #=======================================================================
        # Make sure a world exists.
        # If there is no world yet, then there can be no waypoints.
        elif not self.get_world_size():
            raise BadMsgError("Error: A world must be created before any other objects.")
            #print ("Error: A world must be created before any other objects.")
            #return False  # Error was reported, so no need to return an error flag.

        #=======================================================================
        # Human
        elif arg_list[0]=='human':

            # If the number of arguments is wrong
            if len(arg_list) != 4:
                raise BadLineError
                #return True # Return an error code.
            # If the name is not alphanumeric
            elif not arg_list[1].isalnum():
                raise BadMsgError("Error: Name must be alphanumeric.")
                #print("Error: Name must be alphanumeric.")
                #return False
            #if the name exists already in humans
            elif self.get_human(arg_list[1]):
                raise BadMsgError("Error: Human already exists with that name.")
               # print("Error: Human already exists with that name.")
                #return False
            #if the name exists in robots
            elif self.get_robot(arg_list[1]):
                raise BadMsgError("Error: Robot already exists with that name.")
                #print("Error: Robot already exists with that name.")
                #return False
            #if the name exists in fires
            elif self.get_object(arg_list[1]):
                raise BadMsgError("Error: Fire already exists with that name.")
                #print("Error: Fire already exists with that name.")
                #return False
            # If <x> <y> is not a valid location
            elif not self.get_valid_location(arg_list[2], arg_list[3]):
                raise BadMsgError("Error: Invalid location.")
                #print("Error: Invalid location.")
                #return False
            # Else this appears to be a valid input, and so
            #   create a human at the location.
            else:
                new_location = self.get_valid_location(arg_list[2], arg_list[3])
                print("Creating human ", arg_list[1].capitalize(), " at location ", new_location,".", sep='')
                self.__humans.append(Human(arg_list[1].lower(), new_location))
                #Add humans to the view
                self.__view.update_object(arg_list[1], new_location)


        #if its 'robot':
        elif arg_list[0] == 'robot':

            #If the number of args is wrong
            if len(arg_list) != 4:
                return True  # Return an error code.
            # If the name is not alphanumeric
            #elif not arg_list[1].isalnum():
                #print("Error: Name must be alphanumeric.")
               # return False
            #check for duplicates
            elif self.get_robot(arg_list[1]):
                raise BadMsgError("Error: Robot already exists with that name.")
                #print("Error: Robot already exists with that name.")
                #return False
            # if the name exists already in humans
            elif self.get_human(arg_list[1]):
                raise BadMsgError("Error: Human already exists with that name.")
                #print("Error: Human already exists with that name.")
                #return False
            # if the name exists in fires
            elif self.get_object(arg_list[1]):
                raise BadMsgError("Error: Fire already exists with that name.")
                #print("Error: Fire already exists with that name.")
                #return False
            # If <x> <y> is not a valid location
            elif not self.get_valid_location(arg_list[2], arg_list[3]):
                raise BadMsgError("Error: Invalid location.")
                #print("Error: Invalid location.")
                #return False
            # Else this appears to be a valid input, and so
            #   create a robot at the location.
            else:
                new_location = self.get_valid_location(arg_list[2], arg_list[3])
                print("Creating robot ", arg_list[1].capitalize(), " at location ", new_location, ".", sep='')
                self.__robots.append(Robot(arg_list[1].lower(), new_location))
                # Add robots to the view
                self.__view.update_object(arg_list[1], new_location)

        #if its 'waypoint':
        elif arg_list[0] == 'waypoint':

            # If the number of args is wrong
            if len(arg_list) != 4:
                raise BadLineError
                #return True  # Return an error code.
            # If the name is not alphanumeric
            #elif not arg_list[1].isalnum():
               # print("Error: Name must be alphanumeric.")
                #return False
            #check if waypoint name is one letter
            elif len(arg_list[1]) > 1:
                raise BadMsgError('Error: Waypoint name must be one letter.')
               # print('Error: Waypoint name must be one letter.')
               # return False
            #check if name exists already
            elif self.get_object(arg_list[1]):
                raise BadMsgError('Error: Waypoint already exists with that name.')
               # print('Error: Waypoint already exists with that name.')
               # return False
            # If <x> <y> is not a valid location
            elif not self.get_valid_location(arg_list[2], arg_list[3]):
                raise BadMsgError("Error: Invalid location.")
               # print("Error: Invalid location.")
                #return False
            # check if waypoint has location already
            elif self.check_waypoint_location(self.get_valid_location(arg_list[2], arg_list[3])):
                raise BadMsgError("Error: Waypoint already exists at that location.")
                #print("Error: Waypoint already exists at that location.")
                #return False
            # Else this appears to be a valid input, and so
            #   create a waypoint at the location.
            else:
                new_location = self.get_valid_location(arg_list[2], arg_list[3])
                print("Creating waypoint ", arg_list[1].capitalize(), " at location ", new_location, sep='')
                self.__objects.append(Waypoint(arg_list[1].lower(), new_location))
                # Add waypoints to the view
                self.__view.add_landmark(arg_list[1], new_location)

        #if its 'fire':
        elif arg_list[0] == 'fire':

            # If the number of args is wrong
            if len(arg_list) != 4:
                raise BadLineError
                #return True  # Return an error code.
            # If the name is not alphanumeric
            #elif not arg_list[1].isalnum():
                #print("Error: Name must be alphanumeric.")
                #return False
            # check for duplicates in fires
            elif self.get_object(arg_list[1]):
                raise BadMsgError("Error: Fire already exists with that name.")
                #print("Error: Fire already exists with that name.")
                #return False
            #check for duplicates in humans
            elif self.get_human(arg_list[1]):
                raise BadMsgError("Error: Human already exists with that name.")
                #print("Error: Human already exists with that name.")
                #return False
            # check for duplicates in robots
            elif self.get_robot(arg_list[1]):
                raise BadMsgError("Error: Robot already exists with that name.")
                #print("Error: Robot already exists with that name.")
                #return False
            # If <x> <y> is not a valid location
            elif not self.get_valid_location(arg_list[2], arg_list[3]):
                raise BadMsgError("Error: Invalid location.")
                #print("Error: Invalid location.")
                #return False
            # Else this appears to be a valid input, and so
            #   create a fire at the location.
            else:
                new_location = self.get_valid_location(arg_list[2], arg_list[3])
                print("Creating fire ", arg_list[1].capitalize(), " at location ", new_location, ".", sep='')
                self.__objects.append(Fire(arg_list[1].lower(), new_location))
                # Add fires to the view
                self.__view.update_object(arg_list[1], new_location)
        #=======================================================================
        # Invalid create command.
        else:
            # print(invalid_create_command_string)
            raise BadLineError
            #return True # Return Error flag


    def notify_location(self,name,location):
        #call view and update object
        #NEED TO MAKE WORK FOR WAYPOINTS TOO!!!
        self.__view.update_object(name,location)

    def get_waypoint_location(self,name):
        for i in self.__objects:
            if i.get_name() == name:
                return i.get_location()
        return None


    def check_waypoint_location(self, location):
        for i in self.__objects:
            if i.get_location() == location:
                return i.get_location
            else:
                return None

    def get_robot(self,name):
        for i in self.__robots:
            if i.get_name() == name:
                return i
        else:
            return None

    def get_object(self,name):
        for i in self.__objects:
            if i.get_name() ==  name:
                return i
        else:
            return None


    #===========================================================================
    def get_human(self, name):
        '''
        # Takes a name string.  Looks for a human with that name.  If one exists,
        #   returns that human.  If one does not, returns None.

        Parameters: name, a string
        Returns:    Either a pointer to a human object, or False
        '''
        for i in self.__humans:
            if i.get_name() == name:
                return i
        else:
            return None

    #===========================================================================
    def describe_all(self):
        '''
        Each of the simulation objects describes itself in text.

        ( ) -> None
        '''
        print("The contents of the world are as follows:")
        if self.get_world_size():
            print ("The world is of size ", self.get_world_size(), ".", sep='')
        for i in self.__objects:
            #if its a waypoint
            if i.get_class_name()=='Waypoint':
                print(i)
        for i in self.__humans:
            print(i)
        for i in self.__robots:
            print(i)
        for i in self.__objects:
            #if its a fire
            if i.get_class_name()=='Fire':
                print(i)


#create global model for notify_location()
global the_model
the_model = Model()

class SimObject:
    #init name and location
    def __init__(self,name,location):
        self._name = name
        self._location = location

    def __str__(self):
        return self._name.capitalize() + ' at location ' + str(self._location)

    def get_name(self):
        return self._name

    def get_class_name(self):
        #if its Human
        if type(self)== Human:
            return 'Human'
        #if its Waypoint
        elif type(self)== Waypoint:
            return 'Waypoint'
        #if its Robot
        elif type(self)== Robot:
            return 'Robot'
        #if its Fire
        elif type(self)== Fire:
            return 'Fire'

    def get_location(self):
        #print(self._location)
        return self._location


class Waypoint(SimObject):
    def __str__(self):
        return 'Waypoint ' + super().__str__()


class Traveler(SimObject):

    def __init__(self,name,location):
        self._name = name
        self._location = location
        self._new_location = None
        self._destination_list = [] # a list of locations discussed below in move_to( ).
        self._moving = False #Boolean. Indicates whether the traveler is currently moving. The traveler’s “moving status”

    def set_moving(self):
        #sets the traveler status object status to moving
        self._moving = True


    def journey_to(self,destination_list):
        #see if each location is valid

        for i in destination_list:

            if the_model.get_valid_location(i) == None:

                raise BadMsgError('Error: '+ str(i) +' is not an valid location for this "move"')
        #see if first move is valid
        self.is_valid_move(self._location,destination_list[0])

        #if there are more destinations, see if they are valid
        if len(destination_list)>1:
            for i in range(len(destination_list)):
                try:
                    self.is_valid_move(destination_list[i],destination_list[i+1])
                except IndexError:
                    pass


        self._destination_list = destination_list


        self._moving == True

    def is_valid_move(self,location1, location2):

        # extract x and y coordinates from location 1
        #print(type(location1),type(location2))
        #print(type(location1[0]))
        x = location1[0]
        y = location1[1]
        #extract x and y coordinates from location 2
        new_x = location2[0]
        new_y = location2[1]

        #if none of the current x's or y's match, its an invalid move
        if x != new_x and y != new_y:
            raise BadMsgError('Error: '+ str(location2)+' is not an valid location for this "move"')
        #else one of the x's or y's match, valid move

        return location2




    def get_next_moving_location(self):
        #If the traveler is currently moving, this function returns the location (as a tuple
        #of two integers) that the traveler would move to if it were to move one grid unit closer to the next
        #location in its destination list

        if self._location != self._destination_list[0]:
            # if x's are same
            if self._location[0] == self._destination_list[0][0]:
                # see if next y is less than current
                if self._location[1] > self._destination_list[0][1]:
                    new_location = (self._location[0], self._location[1] - 1)
                # if next y is more than current
                elif self._location[1] < self._destination_list[0][1]:
                    new_location = (self._location[0], self._location[1] + 1)

            # if y's are the same
            if self._location[1] == self._destination_list[0][1]:
                # see if next y is less than current
                if self._location[0] > self._destination_list[0][0]:
                    new_location = (self._location[0] - 1, self._location[1])
                # if next y is more than current
                elif self._location[0] < self._destination_list[0][0]:
                    new_location = (self._location[0] + 1, self._location[1])
        else:
            new_location = self._location
        return new_location



    def stop(self):
        #if the object is moving, stop it
        pass


    def move_to(self,location):

         if location == self._destination_list[0]:
             self._destination_list.remove(location)

         if len(self._destination_list) == 0:
             self._moving = False

         if self._moving == False:
            self._new_location = location
            print(self.get_class_name() + ' ' + self._name.capitalize() + ' at location '+ str(self._location)+
               ' moving to ' + str(self._new_location))

         if self._moving == True:
             self._location = self._new_location
             the_model.notify_location(self._name,self._location)
             print(self.get_class_name() + ' '+ self._name.capitalize() + " moved to location " + str(self._location) + ".")

class Human(Traveler):

    def update(self):

        pass

    def __str__(self):
        return "Human " + super().__str__()

class Robot(Traveler):

    def __init__(self,name,location):
        self._name = name
        self._location = location
        self._extinguishing_fire = None
        self._new_location = None
        self._destination_list = []  # a list of locations discussed below in move_to( ).
        self._moving = False  # Boolean. Indicates whether the traveler is currently moving. The traveler’s “moving status”

    def set_moving(self):
        self._moving = True
        pass

    def stop_fighting_fire(self, fire_object):
        #if the robot is currently fighting this fire, stop fighting it. Else do nothing.
        pass

    def fight_fire(self,fire_object):
        #Set up the robot to extinguish the fire_object.
        pass

    def update(self):
        #If the robot is moving, move the robot one grid unit. If it is extinguishing a fire, reduce that fire
        # object’s “fire strength” by one unit.
        pass

    #def journey_to(self, destination_list):
        #Same as Traveler. journey_to( )
        #this may not be needed?
        #pass

    def __str__(self):
        return 'Robot ' + super().__str__()

class Fire(SimObject):

    def __init__(self,name,location):
        self._name = name
        self._location = location
        _strength = 5 #init to 5, when it reaches 0 delete it from simulation

    def __del__(self):
        # Outputs to the console “Fire <name> has disappeared from the simulation.” <name> is the fire name
        pass

    def get_strength(self):
        #returns the value of _strength
        return self._strength

    def reduce_strength(self):
        #Decrement _strength by 1. If _strength reaches 0, remove fire from the simulation, both from the
        # model and the view
        pass

    def update(self):
        #does nothing
        pass

    def __str__(self):
        return 'Fire ' + super().__str__()


#===============================================================================
# class Human:
#     '''
#     A human in the simulation.
#     '''
#
#     def __init__(self, name, location):
#         # Private member variables.
#         self.__name = name.lower()
#         self.__location = location  # a tuple of integers
#
#     def __str__(self):
#         return "Human " + self.__name.capitalize() + " at location " + str(self.__location)
#
#     def get_name(self):
#         return self.__name
#
#     def move_to(self, location):
#         '''
#         valid location in the world as a tuple of ints -> ( )
#         '''
#         self.__location = location
#         print( "Human " + self.__name.capitalize() + " moved to location " + str(self.__location) + ".")


