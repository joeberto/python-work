
import maya.cmds as cmds
import Mesh as mm

from config import *

#_______________________________________________________________________________
class PenroseUI(object):


#_______________________________________________________________________________
    def __init__(self):

        self._mesh = mm.Mesh()

#_______________________________________________________________________________
    def startUI(self):
        """ Launch the UI. """

        print('Starting PenroseUI')
        # remove any old UIs
        currentWindows = cmds.lsUI(windows=True)
        for window in currentWindows:
            title = cmds.window(window, query=True, title=True)
            if title == 'Penrose Triangle':
                cmds.deleteUI(window)

        self._mainWindow = cmds.window(
            title='Penrose Triangle', width=270, menuBar=True, maximizeButton=False)

        cmds.rowColumnLayout()
        cmds.button(
            'Rotate one step left',
            backgroundColor=MOAVE,
            command=lambda *args: self._rotateOneStepLeft())

        cmds.button(
            'Rotate one step right',
            backgroundColor=MOAVE,
            command=lambda *args: self._rotateOneStepRight())

        cmds.button(
            'Reset View',
            backgroundColor=MOAVE,
            command=lambda *args: self._resetView())

        cmds.button(
            'Play Animation',
            backgroundColor=MOAVE,
            command=lambda *args: self._playAnimation())


        cmds.showWindow()


#_______________________________________________________________________________
    def _rotateOneStepLeft(self):
        """Rotate by one step"""

        self._mesh.rotateOneStepLeft()

    def _rotateOneStepRight(self):
        """Rotate by one step"""

        self._mesh.rotateOneStepRight()

    def _rotateConstant(self):
        """another operation. """

        self._mesh.rotateConstant()

    def _rotateStop(self):

        self._mesh.rotateStop()

    def _resetView(self):
        self._mesh.resetView()

    def _playAnimation(self):
        self._mesh.playAnimation()


