import maya.cmds as cmds

startPT = (0, 0, 0)
startSize = 100.0
startRecursionLevel = 6.0
goldNode = cmds.shadingNode('aiStandardSurface', asShader=True, name='aiSurfNew')
cmds.setAttr(goldNode + '.baseColor', 0.2, 0.1, 0.0, type="double3")
cmds.setAttr(goldNode + '.specularColor', 1.0, 1.0, 0.0, type="double3")
cmds.setAttr(goldNode + '.base', 1.0)

saphNode = cmds.shadingNode('aiStandardSurface', asShader=True, name='aiSurfNew')
cmds.setAttr(saphNode + '.baseColor', 0.0, 0.0, 0.1, type="double3")
cmds.setAttr(saphNode + '.specularColor', 1.0, 1.0, 1.0, type="double3")
cmds.setAttr(saphNode + '.base', 1.0)

emNode = cmds.shadingNode('aiStandardSurface', asShader=True, name='aiSurfNew')
cmds.setAttr(emNode + '.baseColor', 0.0, 0.1, 0.0, type="double3")
cmds.setAttr(emNode + '.specularColor', 1.0, 1.0, 1.0, type="double3")
cmds.setAttr(emNode + '.base', 1.0)

redPlastic = cmds.shadingNode('aiStandardSurface', asShader=True, name='aiSurfNew')
cmds.setAttr(redPlastic + '.baseColor', 0.5, 0.0, 0.0, type="double3")
cmds.setAttr(redPlastic + '.specularColor', 0.0, 0.1, 0.1, type="double3")

bluePlastic = cmds.shadingNode('aiStandardSurface', asShader=True, name='aiSurfNew')
cmds.setAttr(bluePlastic + '.baseColor', 0.0, 0.1, 0.5, type="double3")
cmds.setAttr(bluePlastic + '.specularColor', 0.0, 0.1, 0.1, type="double3")

greenPlastic = cmds.shadingNode('aiStandardSurface', asShader=True, name='aiSurfNew')
cmds.setAttr(greenPlastic + '.baseColor', 0.0, 0.8, 0.1, type="double3")
cmds.setAttr(greenPlastic + '.specularColor', 0.0, 0.1, 0.1, type="double3")
# class Fractal():
#     def __init__(self):
#         self._cube=None

def generateFractal(center, size, recursion, shader):
    recursion -= 1

    if recursion < 0:
        return

    _cube = cmds.polyCube(w=size, h=size, d=size, sx=1, sy=1, sz=1, ax=(1, 0, 0), cuv=4, ch=1)[0]

    cmds.select(_cube)
    cmds.hyperShade(assign=shader)
    cmds.select(clear=True)

    cmds.move(center[0], center[1], center[2], _cube)
    nextSize = size/2.0
    offset = size/4.0 + nextSize/2.0
    right = (center[0] + offset, center[1], center[2])
    left = (center[0] - offset, center[1], center[2])

    front = (center[0], center[1] + offset, center[2])
    back = (center[0], center[1] - offset, center[2])

    top = (center[0], center[1], center[2] + offset)
    bottom = (center[0], center[1], center[2] - offset)

    generateFractal(right, nextSize, recursion, goldNode)
    generateFractal(left, nextSize, recursion, emNode)
    generateFractal(front, nextSize, recursion, saphNode)
    generateFractal(back, nextSize, recursion, redPlastic)
    generateFractal(top, nextSize, recursion, bluePlastic)
    generateFractal(bottom, nextSize, recursion, greenPlastic)

#generateFractal(startPT, startSize, startRecursionLevel, goldNode)

def generateSphereFractal(center, size, recursion, shader):
    recursion -= 1

    if recursion < 0:
        return

    _sphere = cmds.polySphere(r=5, sx=size, sy=size, ax=(1, 0, 0), cuv=2, ch=1)[0]

    cmds.select(_sphere)
    cmds.hyperShade(assign=shader)
    cmds.select(clear=True)

    cmds.move(center[0], center[1], center[2], _sphere)
    nextSize = size/2.0
    offset = size/4.0 + nextSize/2.0
    right = (center[0] + offset, center[1], center[2])
    left = (center[0] - offset, center[1], center[2])

    front = (center[0], center[1] + offset, center[2])
    back = (center[0], center[1] - offset, center[2])

    top = (center[0], center[1], center[2] + offset)
    bottom = (center[0], center[1], center[2] - offset)

    generateSphereFractal(right, nextSize, recursion, goldNode)
    generateSphereFractal(left, nextSize, recursion, emNode)
    generateSphereFractal(front, nextSize, recursion, saphNode)
    generateSphereFractal(back, nextSize, recursion, redPlastic)
    generateSphereFractal(top, nextSize, recursion, bluePlastic)
    generateSphereFractal(bottom, nextSize, recursion, greenPlastic)

generateSphereFractal(startPT, startSize, startRecursionLevel, goldNode)